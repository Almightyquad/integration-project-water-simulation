﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particleSystemController : MonoBehaviour {

    public UnityEngine.ParticleSystem partSys;
    public float mass;
    public float radius;
	UnityEngine.ParticleSystem.Particle[] particles;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void LateUpdate () {
        InitializeIfNeeded();
        int particlesAlive = partSys.GetParticles(particles);
        particlesAlive = partSys.particleCount;
        for (int i = 0; i < particlesAlive; i++)
        {
            for (int j = 0; j < particlesAlive; j++)
            {
                nbody(ref particles[i], ref particles[j]);
            }
        }
        partSys.SetParticles(particles, particlesAlive);
    }

    void nbody(ref UnityEngine.ParticleSystem.Particle particle1, ref UnityEngine.ParticleSystem.Particle particle2)
    {
        Vector2 vec2 = particle1.position - particle2.position;
        float dist = (particle1.position - particle2.position).magnitude;
        if (dist < radius)
        {
            float angleInDegrees = Mathf.Atan2(vec2.x, vec2.y)*180/Mathf.PI;
            particle2.velocity += new Vector3(Mathf.Sin(angleInDegrees)*10, Mathf.Cos(angleInDegrees) * 10, 0);
            particle1.velocity += new Vector3(-Mathf.Sin(angleInDegrees) * 10, -Mathf.Cos(angleInDegrees) * 10, 0);
        }
        
    }

    void InitializeIfNeeded()
    {
        if (partSys == null)
            partSys = GetComponent<UnityEngine.ParticleSystem>();

        if (particles == null || particles.Length < partSys.main.maxParticles)
            particles = new UnityEngine.ParticleSystem.Particle[partSys.main.maxParticles];
    }

}
