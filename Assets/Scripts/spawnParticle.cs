﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnParticle : MonoBehaviour {

    public int numberOfParticles;
    public GameObject particle;
    [HideInInspector]
    public List<GameObject> particles;
	public float spawnRangeScaling;
	// Use this for initialization
	void Start () {
        StartCoroutine(spawnParticles());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator spawnParticles()
    {
        Vector2 newPos = new Vector2(0, 0);
        for (int i = 0; i < numberOfParticles; i++)
        {
            newPos.x = Random.Range(1f, 100f * spawnRangeScaling) / 100f - (spawnRangeScaling/2);
            newPos.y =  12 + (Random.Range(1f, 100f * spawnRangeScaling) / 100f - (spawnRangeScaling/2));
            particles.Add(Instantiate(particle));
            particles[particles.Count - 1].transform.position = newPos;

			if ((i % 100) == 0)
            {
				yield return new WaitForSeconds(0.5f);
            }
        }
    }
}
