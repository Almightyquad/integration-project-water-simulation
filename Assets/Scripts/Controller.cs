﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SharpBox2D.Callbacks;
using SharpBox2D.Collision;
using SharpBox2D.Collision.Shapes;
using SharpBox2D.Common;
using SharpBox2D.Dynamics;
using SharpBox2D.Dynamics.Contacts;
using SharpBox2D.Dynamics.Joints;
using SharpBox2D.Particle;
public class Controller : MonoBehaviour {

	bool b1 = true;
	bool b2 = false;

	int circleIDs = 0;
	int wallIDs = 0;

	public World world;
	Vec2 gravity = new Vec2(0.0f, -10.0f);
	BodyDef groundBodyDef;
	Body groundBody;
	PolygonShape groundBox;

	BodyDef bodyDef;
	Body body;
	PolygonShape dynamicBox;
	FixtureDef fixtureDef;

	float timeStep = 1.0f / 60.0f;
	int velocityIterations = 6;
	int positionIterations = 2;
	SharpBox2D.Particle.ParticleSystem partSystem;
	ParticleGroupDef particleGroupDef;
	PolygonShape particleShape;

	List<GameObject> particles;
	public GameObject particle;
	GameObject particleParent;
	CircleShape circle;
	public GameObject circleBody;
	List<GameObject> circleBodies;
	public float circleRadius;
	public float circleDensity;
	List<Pair<string, Body>> bodyList;
	public GameObject Square;
	List<GameObject> walls;
	public float partRadius;
	public Vector2 partBoxSize;
	public string sceneToLoad;


	void Awake ()
	{
		bodyList = new List<Pair<string, Body>>();
		circleBodies = new List<GameObject>();
		walls = new List<GameObject>();
		world = new World(gravity);
	}

    void Start ()
	{

		createWalls(0.0f, -15.0f, 50.0f, 10.0f, 1.0f,0f);
		createWalls(40.0f, 30.0f, 10.0f, 50.0f, 1.0f,0f);
		createWalls(-40f, 30.0f, 10.0f, 50.0f, 1.0f,0f);
		



		partSystem = new SharpBox2D.Particle.ParticleSystem(world);
		partSystem.setParticleRadius(partRadius);
		partSystem.setParticleDamping(0.2f);
		partSystem.setParticleGravityScale(1f);
		partSystem.setParticleDensity(1.2f);
		//partSystem.m_particleDiameter = 0.5f;

		//createParticleGroup(partBoxSize.x, partBoxSize.y, 0f, partBoxSize.y / 2, partRadius);

		particles = new List<GameObject>();
		particleParent = Instantiate(new GameObject());
		particleParent.name = "particleParent";
		//tempGo.transform.position 
		
		
	}
	
	// Update is called once per frame
	void Update () {
		//Time.deltaTime creates some yitters if there is a large step, especially with slow physics calculations.
		world.step(/*Time.deltaTime*/ timeStep*2, velocityIterations, positionIterations);

		//Vec2 position = body.getPosition();
		//float angle = body.getAngle();
		//print(position.x + " " + position.y + " " + angle);
		if (b2)
		{
			var posBuffere = world.getParticlePositionBuffer();
			Debug.Log(posBuffere [0].x + " " + posBuffere [0].y);
			b2 = false;
		}
		if (b1)
		{
			Vec2 [] posBuffere = partSystem.getParticlePositionBuffer();
			Debug.Log(posBuffere [0].x + " " + posBuffere [0].y);
			b2 = true;
			b1 = false;
			Debug.Log(world.getParticleCount());
		}


		Vec2 [] posBuffer = world.getParticlePositionBuffer();
		for (int i = 0; i < particles.Count; i++)
		{
			particles [i].transform.position = new UnityEngine.Vector2(posBuffer [i].x, posBuffer [i].y);
			particles [i].transform.SetParent(particleParent.transform);

		}

		if (world.getParticleCount() <= particles.Count)
		{
			int tempVar = world.getParticleCount() - particles.Count;
			for (int i = 0; i < tempVar; i++)
			{
				particles.RemoveAt(particles.Count);
			}
		}
		//There is something wrong with this code
		if (world.getParticleCount() > particles.Count)
		{
			int tempVar = world.getParticleCount() - particles.Count;
			for (int i = 0; i < tempVar; i++)
			{
				particles.Add(Instantiate(particle));
				particles [i].transform.localScale = new Vector2(partRadius * 2, partRadius* 2);
				//particles [i].GetComponent<SpriteRenderer>().material.color = particleColor;
			}
		}

		//UnityEngine.Debug.Log(world.getBodyCount());
		//Body bod = world.getBodyList();
		//Debug.Log(bod.getPosition());
		//bod = bod.m_next;
		//Debug.Log(bod.getPosition());

		if (Input.GetMouseButtonUp(0))
		{
			Vector3 tempVec3 = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z);
			Vector2 mousePos = Camera.main.ScreenToWorldPoint(tempVec3);
			Debug.Log(mousePos);
			createCircle(mousePos.x, mousePos.y, circleRadius, circleDensity, 0.3f);
		}

		if (Input.GetKeyUp(KeyCode.Space))
		{
			SceneManager.LoadScene(sceneToLoad);
		}

		for (int i = 0; i < circleBodies.Count; i++)
		{
			Body tempBody = bodyList.Find(ID => ID.first == circleBodies [i].name).second;
			circleBodies[i].transform.position = new Vector2(tempBody.getPosition().x, tempBody.getPosition().y);
		}

		//circleBody.transform.position = new Vector2(body.getPosition().x, body.getPosition().y);//+10);


	}

	void createCircle (float xPos, float yPos, float radius, float density, float friction)
	{
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.position.set(xPos, yPos);
		body = world.createBody(bodyDef);
		circle = new CircleShape();
		circle.m_radius = radius;
		fixtureDef = new FixtureDef();
		fixtureDef.shape = circle;
		fixtureDef.density = density;
		fixtureDef.friction = friction;
		body.setGravityScale(1.0f);
		body.createFixture(fixtureDef);
		bodyList.Add(new Pair<string, Body>("circle" + circleIDs, body));
		GameObject tempGO = Instantiate(circleBody);
		tempGO.name = "circle" + circleIDs;
		//Unity scale is diameter and we want the circle to be rendered in the front, therefore the Z is -0.01f
		tempGO.transform.localScale = new UnityEngine.Vector3(radius * 2, radius * 2);
		circleBodies.Add(tempGO);
		circleIDs++;
	}


	public void createWalls (float xPos, float yPos, float xBoxSize, float yBoxSize, float density, float angle)
	{
		groundBodyDef = new BodyDef();
		groundBodyDef.setAngle(Mathf.Deg2Rad * angle);
		groundBodyDef.position.set(xPos, yPos);
		groundBody = world.createBody(groundBodyDef);
		groundBox = new PolygonShape();
		groundBox.setAsBox(xBoxSize, yBoxSize);
		groundBody.createFixture(groundBox, density);
		bodyList.Add(new Pair<string, Body>("wall" + wallIDs, groundBody));
		GameObject tempGO = Instantiate(Square);
		tempGO.name = "wall" + wallIDs;
		tempGO.transform.position = new Vector2(xPos, yPos);
		tempGO.transform.localScale = new Vector2(xBoxSize*2, yBoxSize*2);
		//This code is stupid, why is the rotation in Box2D not 45 degrees?
		tempGO.transform.Rotate(0, 0, angle);
		walls.Add(tempGO);
		wallIDs++;
		/*groundBodyDef.position.set(50, 0);
		groundBody = world.createBody(groundBodyDef);
		groundBox.setAsBox(10.0f, 50.0f);
		groundBody.createFixture(groundBox, 1.0f);
		groundBodyDef.position.set(-50, 0);
		groundBody = world.createBody(groundBodyDef);
		groundBox.setAsBox(10.0f, 50.0f);
		groundBody.createFixture(groundBox, 0.0f);*/
	}

	public void createParticleGroup (float xSpawnBoxSize, float ySpawnBoxSize, float xPos, float yPos, float particleRadius)
	{
		particleShape = new PolygonShape();
		particleShape.setAsBox(xSpawnBoxSize, ySpawnBoxSize);
		particleGroupDef = new ParticleGroupDef();
		particleGroupDef.flags = ParticleType.b2_waterParticle;
		particleGroupDef.position.set(xPos, yPos);
		particleGroupDef.shape = particleShape;
		world.setParticleRadius(particleRadius);
		partSystem.createParticleGroup(particleGroupDef);
		world.createParticleGroup(particleGroupDef);

		
	}

}
