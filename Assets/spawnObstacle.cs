﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnObstacle : MonoBehaviour {

	Controller controller;
	
	// Use this for initialization
	void Start () {
		controller = Camera.main.GetComponent<Controller>();

        controller.createWalls(this.transform.position.x, this.transform.position.y, this.transform.localScale.x/2, this.transform.localScale.y/2, 1f, this.transform.eulerAngles.z);
	}
	
	// Update is called once per frame
	void Update () {
		Destroy(this.gameObject);
	}
}
