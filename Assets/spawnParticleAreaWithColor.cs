﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnParticleAreaWithColor : MonoBehaviour {

	Controller controller;
	// Use this for initialization
	void Start () {
		controller = Camera.main.GetComponent<Controller>();
	}
	
	// Update is called once per frame
	void Update () {
		if (controller.world != null)
		{
			controller.createParticleGroup(this.transform.localScale.x / 2, this.transform.localScale.y / 2, this.transform.position.x, this.transform.position.y, controller.partRadius);
			
			Destroy(this.gameObject);
		}
	}
}
